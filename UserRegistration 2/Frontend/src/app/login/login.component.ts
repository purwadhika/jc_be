import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string = "";
  password:string = "";

  constructor(private http:Http, private router:Router) { }

  ngOnInit() {
  }

  login(){
    let obj = {
      "email" : this.email,
      "password" : this.password
    };

    let body = JSON.stringify(obj);
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    this.http.post('http://localhost:8000/api/user/login', body, options)
      .subscribe(
        result => {
            this.router.navigate(['/user']);
        },
        err => {
            this.router.navigate(['/register']);
        }
      );
  }

}
