<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Simple Commerce</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <img alt="Brand" src="...">
                    </a>
                </div>
            </div>
        </nav>
        <div>
        </div>
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-4 col-md-4">
                    <table class="table table-bordered">
                        <tr>
                            <th>Order List</th>
                        </tr>
                        <script>
                            function addSubstractItem(id, url, token)
                            {
                                $.post(url,
                                    {
                                        "_token": token,
                                        "product_id": id
                                    },
                                    function(){
                                        window.location.replace("/");
                                    }
                                );
                            }
                        </script>
                        @foreach($orderList as $o)
                        <tr>
                            <td>
                                {{ $o['name'] }} | Rp. {{ $o['price'] }} | {{ $o['qty'] }}
                                <div class="pull-right">
                                    <button class="btn btn-primary" type="button" onclick="addSubstractItem({{ $o['product_id'] }}, '/addOrderQty', '{{ csrf_token() }}')" >+</button>
                                    <button class="btn btn-primary" type="button" onclick="addSubstractItem({{ $o['product_id'] }}, '/subtractOrderQty', '{{ csrf_token() }}')">-</button>
                                </div> 
                            </td>
                        </tr>
                        @endforeach


                    </table>
                </div>

                <div class="col-sm-8 col-md-8">
                    <div class="row">
                        @foreach($productList as $p)
                            <div class="col-sm-4 col-md-4">
                                <div class="thumbnail">
                                    <img src="{{ $p->image }}" alt="Product">
                                    <div class="caption">
                                        <h5 class="pull-right">Rp. {{ $p->price }}</h5>
                                        <h5>{{ $p->name }}</h5>
                                        <p>{{ $p->description }}</p>
                                        <script>
                                            function buyItem(id, token)
                                            {
                                                $.post('/buy',
                                                    {
                                                        "_token": token,
                                                        "product_id": id
                                                    },
                                                    function(){
                                                        window.location.replace("/");
                                                    }
                                                );
                                            }
                                        </script>
                                        <button class="btn btn-primary" onclick="buyItem({{ $p->id }}, '{{ csrf_token() }}')">Buy</button>
                                    </div>
                                </div>
                            </div> 
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
    </body>
</html>