<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    function Product()
    {
        return $this->belongsTo('App\Product');
    }
}
