<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    function OrderItem()
    {
        return $this->hasMany('App\OrderItem');
    }
}
