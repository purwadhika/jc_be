<?php

namespace App\Http\Controllers;

use App\Product;
use App\OrderItem;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(){

        $productList = Product::get();
        
        $orderList = array();
        
        foreach (OrderItem::get() as $orderItem) {

            //$p = Product::find($orderItem->product_id);

            $p = $orderItem->product;

            array_push($orderList, 
                [
                    'product_id' => $p->id, 
                    'name' => $p->name, 
                    'price' => $orderItem->price, 
                    'qty' => $orderItem->qty
                ]
            );

        }

        return view('app', ['productList' => $productList, 'orderList' => $orderList]);
    }

    public function addOrderQty(Request $request){
        try{
            $productId = $request->product_id;
            if (!empty($productId)) {
                if (!empty($productId)) {
                    $order = OrderItem::where('product_id', '=', $productId)->first();
                    if(!empty($order)){
                        $order->qty += 1;
                        $order->save();
                    }
                }
            }
            else{
                return response()->json(array('error' => 'product not found'), 404);
            }

            return response()->json(array("message" => 'success !!'), 200);
        }
        catch(\Exception $e){
            return response()->json(array('error' => $e->getMessage()), 500);
        }
    }

    public function subtractOrderQty(Request $request){
        try{
            $productId = $request->product_id;
            if (!empty($productId)) {
                $order = OrderItem::where('product_id', '=', $productId)->first();
                if(!empty($order)){
                    if($order->qty > 1){
                        $order->qty -= 1;
                        $order->save();
                    }
                    else{
                        $order->delete();
                    }
                }
            }
            else{
                return response()->json(array('error' => 'product not found'), 404);
            }

            return response()->json(array("message" => 'success !!'), 200);
        }
        catch(\Exception $e){
            return response()->json(array('error' => $e->getMessage()), 500);
        }
    }

    public function buy(Request $request){
        try{
            $productId = $request->product_id;
        
            if (!empty($productId)) {
                $order = OrderItem::where('product_id', '=', $productId)->first();
                
                if(!empty($order)){
                    $order->qty += 1;
                    $order->save();
                }
                else{
                    $p = Product::find($productId);

                    $order = new OrderItem;
                    $order->product_id = $p->id;
                    $order->price = $p->price;
                    $order->qty = 1;
                    $order->save();
                }
            }
            else{
                return response()->json(array('error' => 'product not found'), 404);
            }

            return response()->json(array("message" => 'success !!'), 200);
        }
        catch(\Exception $e){
            return response()->json(array('error' => $e->getMessage()), 500);
        }
    }


}
