<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UserRegController extends Controller
{
    function index(){
        $user_list = DB::select('select * from user_reg where id = ?', [5]); // raw sql query

        $user_list2 = DB::table('user_reg')
                            ->get(); // query builder


        return view('user', ['user_list' => $user_list2]);
    }

    function add(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|max:255',
        ]);

        // DB::insert('insert into user_reg (name, address, email, created_at, updated_at) values (?, ?, ?, ?, ?)', [$request->name, $request->address, $request->email, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')]);

        DB::table('user_reg')->insert(
            ['name' => $request->name , 'address' => $request->address, 'email' => $request->email , 'created_at' => date('Y-m-d H:i:s') ,'updated_at' => date('Y-m-d H:i:s')]
        );
        $p = new Product;
        $p->name = "New Product 2";
        $p->save();
        
        
        $p = Product::where('id', '=', 1)->first();
        $p->name = "New Product 2";
        $p->save();

        return redirect('/');
    }

    function delete($id){
        //DB::delete('delete from user_reg where id = ?', [$id]);

        DB::table('user_reg')->where('id','=', $id)->delete();

        // DB::table('user_reg')
        // ->where('id','=', $id)
        // ->update([ 'name' => 'AAAA']);

        return redirect('/');
    }
}
