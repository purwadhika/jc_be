<!DOCTYPE html>
<html>
<head>
	<title>User Registration</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<style>
        .user-registration{
            background-color: #e1efef;
            padding: 30px;
            margin-bottom: 10px;
        }
    </style>

    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif


    <div class="container">
        <nav class="navbar navbar-default" style="border-radius: 0; margin-bottom:0">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        User Registration Form
                    </a>
                </div>
            </div>
        </nav>
        <div class="user-registration">
            <form action="/add" method="POST">
                {{ csrf_field() }}

                <div class="form-group row">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" placeholder="Enter your name" name="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" placeholder="Enter your address" name="address">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 control-label" >Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" placeholder="Enter your email" name="email">
                    </div>
                </div>
                <div class="" style="text-align: right">
                    <button type="submit" class="btn btn-promary">Add</button>
                </div>
            </form>
        </div>

        <nav class="navbar navbar-default" style="border-radius: 0; margin-bottom:0">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        List of User
                    </a>
                </div>
            </div>
        </nav>
        <div class="user-list">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>ID.</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                @foreach ($user_list as $u)
                    <tr>
                        <td>{{ $u->id }}</td>
                        <td>{{ $u->name }}</td>
                        <td>{{ $u->address }}</td>
                        <td>{{ $u->email }}</td>
                        <td>
                            <form action="{{ '/delete/' . $u->id }}" method="POST">
                                {{ csrf_field() }}
								<button type="submit" class="btn btn-primary">Remove</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>	

	<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>