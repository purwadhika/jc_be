<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ToDoController extends Controller
{
    function index(){
        $todoList = DB::select('select * from todo');

        return view('todo', ["todolist" => $todoList]);
    }

    function addToDo(Request $request){

        $this->validate($request, [
            'name' => 'required|max:255',
            'category' => 'required|max:255',
        ]);
        
        DB::insert('insert into todo (name, category, date, complete, created_at, updated_at) values (?, ?, ?, ?, ?, ?)', [$request->name, $request->category, date('Y-m-d H:i:s'), false, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')]);

        return redirect("/");
    }

    function deleteToDo(Request $request){
        $completedList = $request->complete;
        if(!empty($completedList))
        {
            foreach ($completedList as $val) {
                DB::delete('delete from todo where id = ?', [$val]);
            }
        }
        return redirect("/");
    }
}
