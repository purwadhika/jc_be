<!DOCTYPE html>
<html>
<head>
	<title>Todo List</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="">
                    Todo List
                </a>
            </div>
        </div>
    </nav>

    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif


    <div class="container col-sm-offset-2 col-sm-8">
		<div class="panel panel-default">
    		<div class="panel-heading">
    			My Todo List
    		</div>
    		<div class="panel-body">
                <form action="/delete" method="POST">
                    {{ csrf_field() }}
                    <table class="table table-striped">
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Date</th>
                            <th>Complete</th>
                        </tr>
                        @foreach ($todolist as $todo)
                            <tr>
                                <td>{{ $todo->name }}</td>
                                <td>{{ $todo->category }}</td>
                                <td>{{ $todo->date }}</td>
                                <td>
                                    <input type="checkbox" name="complete[]" value="{{ $todo->id }}">
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-default">Update Task</button>
                </form>
    		</div>
    	</div>

        <form action="/add" method="POST">
			{{ csrf_field() }}

	    	<div class="panel panel-default">
				<div class="panel-heading">
					New Todo
				</div>
				<div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <input type="text" class="form-control" id="category" name="category">
                    </div>
                    <button type="submit" class="btn btn-default">Add Item</button>
				</div>
			</div>
		</form>

		
    </div>
    	

	<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>